<?php

namespace Drupal\combined_image_style\Controller;

use Drupal\combined_image_style\Entity\CombinedImageStyle;
use Drupal\image\Controller\ImageStyleDownloadController as ImageStyleDownloadControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ImageStyleDownloadController extends ImageStyleDownloadControllerBase {

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param $scheme
   * @param $image_styles
   * @param string $required_derivative_scheme
   *
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function deliverCombined(Request $request, $scheme, $image_styles, string $required_derivative_scheme): Response {
    return $this->deliver($request, $scheme, CombinedImageStyle::fromName($image_styles), $required_derivative_scheme);
  }

}
