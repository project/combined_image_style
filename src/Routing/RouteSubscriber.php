<?php

namespace Drupal\combined_image_style\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    /** @var \Symfony\Component\Routing\Route $route */
    if ($route = $collection->get('image.style_public')) {
      $path = $route->getPath();

      $route->setPath(str_replace('image_style', 'image_styles', $path));
      $route->setDefault('_controller', '\Drupal\combined_image_style\Controller\ImageStyleDownloadController::deliverCombined');
    }
  }

  public static function getSubscribedEvents(): array {
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -1025];
    return $events;
  }

}
