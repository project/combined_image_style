<?php

namespace Drupal\combined_image_style_formatters;

use Drupal\combined_image_style\Entity\CombinedImageStyle;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Template\Attribute;
use Drupal\responsive_image\ResponsiveImageStyleInterface;

trait CombinedImageStyleFormatterTrait {

  public static function defaultSettings(): array {
    return [
      'breakpoint_group' => '',
      'keyed_styles' => [],
      'fallback_image_style' => '',
    ];
  }

  public function settingsSummary(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $image_styles = image_style_options(TRUE);
    $image_styles[ResponsiveImageStyleInterface::ORIGINAL_IMAGE] = $this->t('- None (original image) -');
    $image_styles[ResponsiveImageStyleInterface::EMPTY_IMAGE] = $this->t('- empty image -');

    $breakpoint_group = $this->getSettingFromFormState($form_state, 'breakpoint_group');

    $form['#prefix'] = '<div id="field-formatter-ajax">';
    $form['#suffix'] = '</div>';

    $form['breakpoint_group'] = [
      '#type' => 'select',
      '#title' => $this->t('Breakpoint group'),
      '#default_value' => $breakpoint_group,
      '#options' => $this->breakpointManager->getGroups(),
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'refreshAjax'],
        'wrapper' => 'field-formatter-ajax',
      ],
    ];

    $breakpoints = array_reverse($this->breakpointManager->getBreakpointsByGroup($breakpoint_group));

    if ($breakpoint_group && $breakpoints) {
      $form['keyed_styles'] = [
        '#type' => 'container',
        '#attributes' => [
          'id' => 'responsive-image-style-breakpoints-wrapper',
        ],
      ];

      $defaultStyles = $this->getSetting('keyed_styles');
      $styles = max(0, (int) $form_state->get('styles'), count($defaultStyles) - 1);
      foreach (range(0, $styles) as $delta) {
        $form['keyed_styles'][$delta] = [
          '#type' => 'details',
          '#title' => $this->t('Style @style', ['@style' => $delta + 1]),
        ];

        foreach ($breakpoints as $breakpoint_id => $breakpoint) {
          $breakpoint_id = str_replace('.', '--', $breakpoint_id);

          foreach ($breakpoint->getMultipliers() as $multiplier) {
            $label = $multiplier . ' ' . $breakpoint->getLabel() . ' [' . $breakpoint->getMediaQuery() . ']';

            $form['keyed_styles'][$delta][$breakpoint_id][$multiplier] = [
              '#type' => 'select',
              '#title' => $label,
              '#options' => $image_styles,
              '#default_value' => $defaultStyles[$delta][$breakpoint_id][$multiplier] ?: NULL,
              '#description' => $this->t('Select an image style for this breakpoint.'),
            ];
          }
        }
      }

      $form['fallback_image_style'] = [
        '#title' => $this->t('Fallback image style'),
        '#type' => 'select',
        '#default_value' => $this->getSetting('fallback_image_style'),
        '#options' => $image_styles,
        '#required' => TRUE,
        '#description' => $this->t('Select the smallest image style you expect to appear in this space. The fallback image style should only appear on the site if an error occurs.'),
      ];
      $form['add_more'] = [
        '#type' => 'submit',
        '#value' => 'Add style',
        '#submit' => [[$this, 'addMore']],
        '#ajax' => [
          'callback' => [$this, 'refreshAjax'],
          'wrapper' => 'field-formatter-ajax',
        ],
      ];
    }

    return $form;
  }

  public static function addMore(array &$form, FormStateInterface $form_state): void {
    $styles = $form_state->get('styles');
    $form_state->set('styles', $styles + 1);
    $form_state->setRebuild();
  }

  public static function refreshAjax($form, FormStateInterface $form_state): ?array {
    $triggeringElement = $form_state->getTriggeringElement();
    // Dynamically return the dependent ajax for elements based on the
    // triggering element. This shouldn't be done statically because
    // settings forms may be different, e.g. for layout builder, core, ...
    if (!empty($triggeringElement['#array_parents'])) {
      $subformKeys = $triggeringElement['#array_parents'];
      // Remove the triggering element itself:
      array_pop($subformKeys);
      // Return the subform:
      return NestedArray::getValue($form, $subformKeys);
    }

    return NULL;
  }

  protected function getMimeType(CombinedImageStyle $combinedImageStyle, $extension): string {
    return $this->mimeTypeGuesser->guessMimeType('responsive_image.' . $combinedImageStyle->getDerivativeExtension($extension));
  }

  protected function getSettingFromFormState(FormStateInterface $form_state, $setting) {
    $field_name = $this->fieldDefinition->getName();

    if ($form_state->hasValue(['fields', $field_name, 'settings_edit_form', 'settings', $setting])) {
      return $form_state->getValue(['fields', $field_name, 'settings_edit_form', 'settings', $setting]);
    }

    return $this->getSetting($setting);
  }

  public function viewElement(EntityReferenceItemInterface $item, EntityInterface $file):array {
    $cache_tags = [];
    $sources = [];
    $breakpoints = array_reverse($this->breakpointManager->getBreakpointsByGroup($this->getSetting('breakpoint_group')));
    foreach ($breakpoints as $breakpoint_id => $breakpoint) {
      $breakpoint_id = str_replace('.', '--', $breakpoint_id);
      $multipliers = array_merge_recursive(...array_map(static function (array $keyedStyle) use ($breakpoint_id) {
        return $keyedStyle[$breakpoint_id];
      }, $this->getSetting('keyed_styles')));

      $extension = pathinfo($file->getFileUri(), PATHINFO_EXTENSION);
      $srcset = [];
      $derivative_mime_types = [];
      foreach ($multipliers as $multiplier => $imageStyles) {
        $combinedImageStyle = (new CombinedImageStyle())
          ->setSourceUri($file->getFileUri())
          ->setImageStyles($imageStyles);

        $derivative_mime_type = $this->getMimeType($combinedImageStyle, $extension);
        $derivative_mime_types[] = $derivative_mime_type;

        $srcset[(int) mb_substr($multiplier, 0, -1) * 100] = $combinedImageStyle->buildCombinedUrl() . ' ' . $multiplier;

        $cache_tags = Cache::mergeTags($cache_tags, $combinedImageStyle->getCacheTags());
      }

      $sources[] = new Attribute(array_filter([
        'srcset' => implode(', ', array_unique($srcset)),
        'media' => trim($breakpoint->getMediaQuery()),
        'type' => count(array_unique($derivative_mime_types)) === 1 ? $derivative_mime_types[0] : NULL,
      ]));
    }

    return [
      '#theme' => 'combined_image_style_responsive_image',
      '#sources' => $sources,
      '#image' => array_filter([
        '#theme' => 'image_style',
        '#uri' => $file->getFileUri(),
        '#style_name' => $this->getSetting('fallback_image_style'),
        '#alt' => $item->alt,
        '#title' => $item->title,
        '#attributes' => $item->_attributes,
      ]),
      '#cache' => [
        'tags' => $cache_tags,
      ],
    ];
  }

}
