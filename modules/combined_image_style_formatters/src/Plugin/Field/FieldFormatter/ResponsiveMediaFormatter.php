<?php

namespace Drupal\combined_image_style_formatters\Plugin\Field\FieldFormatter;

use Drupal\breakpoint\BreakpointManagerInterface;
use Drupal\combined_image_style_formatters\CombinedImageStyleFormatterTrait;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\ProxyClass\File\MimeType\ExtensionMimeTypeGuesser;
use Drupal\Core\TypedData\TranslatableInterface;
use Drupal\file\FileInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin for responsive image formatter.
 *
 * @FieldFormatter(
 *   id = "responsive_media_combined_image_style",
 *   label = @Translation("Responsive media (Combined image style)"),
 *   field_types = {
 *     "entity_reference",
 *   }
 * )
 */
class ResponsiveMediaFormatter extends EntityReferenceEntityFormatter {

  use CombinedImageStyleFormatterTrait;

  private BreakpointManagerInterface $breakpointManager;
  private ExtensionMimeTypeGuesser $mimeTypeGuesser;

  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, LoggerChannelFactoryInterface $logger_factory, EntityTypeManagerInterface $entity_type_manager, EntityDisplayRepositoryInterface $entity_display_repository, BreakpointManagerInterface $breakpointManager, ExtensionMimeTypeGuesser $mimeTypeGuesser) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $logger_factory, $entity_type_manager, $entity_display_repository);

    $this->breakpointManager = $breakpointManager;
    $this->mimeTypeGuesser = $mimeTypeGuesser;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('logger.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository'),
      $container->get('breakpoint.manager'),
      $container->get('file.mime_type.guesser.extension')
    );
  }

  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];
    $files = $this->getEntitiesToView($items, $langcode);

    foreach ($files as $delta => $file) {
      $item = $items[$delta];

      if ($file instanceof FileInterface) {
        $elements[$delta] = $this->viewElement($item, $file);
      }
      else {
        $view_builder = $this->entityTypeManager->getViewBuilder('media');
        $elements[$delta] = $view_builder->view($file, 'default');
      }
    }

    return $elements;
  }

  protected function getEntitiesToView(EntityReferenceFieldItemListInterface $items, $langcode): array {
    $entities = [];

    /**
     * @var \Drupal\media\MediaInterface $item
     */
    foreach ($items->referencedEntities() as $delta => $item) {
      if ($entity = $item->get($item->getSource()->configuration['source_field'])->entity) {
        // Set the entity in the correct language for display.
        if ($entity instanceof TranslatableInterface) {
          $entity = \Drupal::service('entity.repository')
            ->getTranslationFromContext($entity, $langcode);
        }

        $access = $this->checkAccess($entity);
        // Add the access result's cacheability, ::view() needs it.
        $item->_accessCacheability = CacheableMetadata::createFromObject($access);
        if ($access->isAllowed()) {
          // Add the referring item, in case the formatter needs it.
          $entity->_referringItem = $items[$delta];
          $entities[$delta] = $entity;
        }
      }
      else {
        $entities[$delta] = $item;
      }
    }

    return $entities;
  }

}
