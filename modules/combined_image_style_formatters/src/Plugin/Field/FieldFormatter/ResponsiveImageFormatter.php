<?php

namespace Drupal\combined_image_style_formatters\Plugin\Field\FieldFormatter;

use Drupal\breakpoint\BreakpointManagerInterface;
use Drupal\combined_image_style_formatters\CombinedImageStyleFormatterTrait;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\ProxyClass\File\MimeType\ExtensionMimeTypeGuesser;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin for responsive image formatter.
 *
 * @FieldFormatter(
 *   id = "responsive_image_combined_image_style",
 *   label = @Translation("Responsive image (Combined image style)"),
 *   field_types = {
 *     "image",
 *   },
 *   quickedit = {
 *     "editor" = "image"
 *   }
 * )
 */
class ResponsiveImageFormatter extends ImageFormatterBase {

  use CombinedImageStyleFormatterTrait;

  private BreakpointManagerInterface $breakpointManager;
  private ExtensionMimeTypeGuesser $mimeTypeGuesser;

  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, BreakpointManagerInterface $breakpointManager, ExtensionMimeTypeGuesser $mimeTypeGuesser) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->breakpointManager = $breakpointManager;
    $this->mimeTypeGuesser = $mimeTypeGuesser;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('breakpoint.manager'),
      $container->get('file.mime_type.guesser.extension')
    );
  }

  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];
    $files = $this->getEntitiesToView($items, $langcode);

    foreach ($files as $delta => $file) {
      $item = $items[$delta];
      $elements[$delta] = $this->viewElement($item, $file);
    }

    return $elements;
  }

}
